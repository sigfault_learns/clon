#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "operations.h"

/**
 * @brief Функция тестирования длинной арифметики
 * @param a - Значение 1 (Слагаемое, Уменьшаемое, Делмое, Множимое)
 * @param b - Значение 2 (Слагаемое, Вычитаемое, Делитель, Множитель)
 * @param testName - Имя теста
 * @param sign - Операция (+, -, *, /)
 */
void test(char *a, char *b, char *testName, char sign)
{
    int ai = atoi(a);   // Значение 1 типа int
    int bi = atoi(b);   // Значение 2 типа int
    long norm;

    /* Результат */
    char result[MAXBYTELEN];
    bzero(result, MAXBYTELEN);

    /* Производим операцию */
    switch(sign)
    {
    case '+':
        addition(a, b, result);
        norm = ai + bi;
        break;
    case '-':
        substraction(a, b, result);
        norm = ai - bi;
        break;
    case '*':
        multiplection(a, b, result);
        norm = ai * bi;
        break;
    case '/':
        division(a, b, result);
        norm = ai / bi;
        break;
    default:
        printf("%s: Wrong sign", testName);
        return;
    }

    /* Выводим результат в консоль */
    printf("%s: %s %c %s = %s (norm %ld)", testName, a, sign, b, result,
                                             norm);

    /* Сравниваем результат с вычислением */
    char normStr[MAXBYTELEN];
    sprintf(normStr, "%ld", norm);

    /* Выводим OK или FAIL в зависимости от результата */
    if (strcmp(normStr, result) == 0)
        printf(" \x1b[33mOK\x1b[0m\n");
    else
        printf(" \x1b[31mFAIL\x1b[0m\n");
}

void test1()
{
    char a[MAXBYTELEN] = "12457841";
    char b[MAXBYTELEN] = "41574852";

    test(a, b, "test1", '+');
}

void test2()
{
    char a[MAXBYTELEN] = "1245174552";
    char b[MAXBYTELEN] = "415748";

    test(a, b, "test2", '+');
}

void test3()
{
    char a[MAXBYTELEN] = "1245";
    char b[MAXBYTELEN] = "41574852";

    test(a, b, "test3", '+');
}

void test4()
{
    char a[MAXBYTELEN] = "12457841";
    char b[MAXBYTELEN] = "41574852";

    test(a, b, "test4", '-');
}

void test5()
{
    char a[MAXBYTELEN] = "1245174552";
    char b[MAXBYTELEN] = "415748";

    test(a, b, "test5", '-');
}

void test6()
{
    char a[MAXBYTELEN] = "1245";
    char b[MAXBYTELEN] = "41574852";

    test(a, b, "test6", '-');
}

void test7()
{
    char a[MAXBYTELEN] = "12457";
    char b[MAXBYTELEN] = "41574";

    test(a, b, "test7", '*');
}

void test8()
{
    char a[MAXBYTELEN] = "124517";
    char b[MAXBYTELEN] = "415";

    test(a, b, "test8", '*');
}

void test9()
{
    char a[MAXBYTELEN] = "1245";
    char b[MAXBYTELEN] = "41574";

    test(a, b, "test9", '*');
}

void test10()
{
    char a[MAXBYTELEN] = "-12457841";
    char b[MAXBYTELEN] = "41574852";

    test(a, b, "test10", '+');
}

void test11()
{
    char a[MAXBYTELEN] = "12457841";
    char b[MAXBYTELEN] = "-41574852";

    test(a, b, "test11", '+');
}

void test12()
{
    char a[MAXBYTELEN] = "-1245174552";
    char b[MAXBYTELEN] = "-415748";

    test(a, b, "test12", '+');
}

void test13()
{
    char a[MAXBYTELEN] = "-12457841";
    char b[MAXBYTELEN] = "41574852";

    test(a, b, "test13", '-');
}

void test14()
{
    char a[MAXBYTELEN] = "1245174552";
    char b[MAXBYTELEN] = "-415748";

    test(a, b, "test14", '-');
}

void test15()
{
    char a[MAXBYTELEN] = "-1245";
    char b[MAXBYTELEN] = "-41574852";

    test(a, b, "test15", '-');
}

void test16()
{
    char a[MAXBYTELEN] = "-12457";
    char b[MAXBYTELEN] = "41574";

    test(a, b, "test16", '*');
}

void test17()
{
    char a[MAXBYTELEN] = "124517";
    char b[MAXBYTELEN] = "-5771";

    test(a, b, "test17", '*');
}

void test18()
{
    char a[MAXBYTELEN] = "-1245";
    char b[MAXBYTELEN] = "-41574";

    test(a, b, "test18", '*');
}

void test19()
{
    char a[MAXBYTELEN] = "10";
    char b[MAXBYTELEN] = "11";

    test(a, b, "test19", '-');
}

void test20()
{
    char a[MAXBYTELEN] = "124517";
    char b[MAXBYTELEN] = "415";

    test(a, b, "test20", '/');
}

void test21()
{
    char a[MAXBYTELEN] = "1245";
    char b[MAXBYTELEN] = "41574";

    test(a, b, "test21", '/');
}

void test22()
{
    char a[MAXBYTELEN] = "914521";
    char b[MAXBYTELEN] = "124514";

    test(a, b, "test22", '/');
}

void test23()
{
    char a[MAXBYTELEN] = "11";
    char b[MAXBYTELEN] = "10";

    test(a, b, "test23", '/');
}

void test24()
{
    char a[MAXBYTELEN] = "-124517";
    char b[MAXBYTELEN] = "415";

    test(a, b, "test24", '/');
}

void test25()
{
    char a[MAXBYTELEN] = "124517";
    char b[MAXBYTELEN] = "-415";

    test(a, b, "test25", '/');
}

void test26()
{
    char a[MAXBYTELEN] = "-124517";
    char b[MAXBYTELEN] = "-415";

    test(a, b, "test26", '/');
}

void test27()
{
    char a[MAXBYTELEN] = "1234";
    char b[MAXBYTELEN] = "12";

    test(a, b, "test27", '/');
}

void test28()
{
    char a[MAXBYTELEN] = "1234";
    char b[MAXBYTELEN] = "1234";

    test(a, b, "test28", '-');
}

int main()
{
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    test9();
    test10();
    test11();
    test12();
    test13();
    test14();
    test15();
    test16();
    test17();
    test18();
    test19();
    test20();
    test21();
    test22();
    test23();
    test24();
    test25();
    test26();
    test27();
    test28();

    return 0;
}
