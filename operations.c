#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "operations.h"

/**
 * @brief Функция разворота строки
 * @param str - Строка
 */
void revert(char *str)
{
    int strLength = strlen(str);    // Длина строки
    char tmp[MAXBYTELEN];          // Временная строка
    int i;                          // Счетчик

    /* Обнуляем временную строку */
    bzero(tmp, MAXBYTELEN);

    /* Заполняем временную строку */
    for(i = 0; i < strLength; i++)
        tmp[i] = str[strLength-i-1];

    /* Заменяем введенную строку, на перевернутую */
    strcpy(str, tmp);
}

/**
 * @brief Функция сравнения чисел записанных в строку
 * @param str1 - Строка 1
 * @param str2 - Строка 2
 * @retval 1 - Первое строчное число больше
 * @retval 0 - Строчные числа равны
 * @retval -1 - Второе строчное число больше
 *
 * @remarks strcmp не подходит, поскольку сравнение идёт
 *          сначало по строке, а потом по размеру
 */
int compare(char *str1, char *str2)
{
    int len1 = strlen(str1);    // Длина строки 1
    int len2 = strlen(str2);    // Длина строки 2

    /* Определяем равенство чисел по длине */
    if (len1 > len2)
        return 1;

    if (len1 < len2)
        return -1;

    /* Длины строк равны */
    /* Определяем равенство чисел по содержимому */
    return strcmp(str1, str2);
}

/**
 * @brief Функция суммирования целых положительных чисел
 * @param addendum1 - Слагаемое 1
 * @param addendum2 - Слагаемое 2
 * @param out - Указатель на вывод
 */
void add(char *addendum1, char *addendum2, char *out)
{
    uint8_t over = 0;   // Переполнение, содержит первый разряд суммы
    int sum = 0;        // Сумма разрядов (содержит нулевой и первый разряды)
    int fLen = strlen(addendum1), sLen = strlen(addendum2); // Длины строк
    int i=0;    // Счетчик

    /* Обнуляем вывод */
    bzero(out, MAXBYTELEN);

    /* Суммируем поразрядно */
    for(i = 0; i < fLen || i < sLen; i++)
    {
        /* Переменные содержат текущий разряд числа или '0',
         * если он не существует */
        uint8_t f = i < fLen ? addendum1[fLen-i-1]-'0' : 0;
        uint8_t s = i < sLen ? addendum2[sLen-i-1]-'0' : 0;

        /* Вычисление суммы */
        sum = f + s + over;

        /* 1 Разряд отправляется в переполнение */
        over = sum / 10;

        /* 0 разряд отправляется в переменную вывода */
        out[i] = (char)(sum % 10) + '0';
    }

    /* Если в переменной переполнения содрежится значение, то оно
     * отправляется в переменную вывода */
    if (over) out[i++] = over + '0';

    /* Разворачиваем число, поскольку оно заполнялось слева направо */
    revert(out);
}

/**
 * @brief Функция вычитания целых полажительных чисел
 * @param minuend - Уменьшаемое
 * @param subtrahend - Вычитаемое
 * @param out - Указатель на вывод
 */
void sub(char *minuend, char *subtrahend, char *out)
{
    int diff = 0;               // Разность разрядов
    uint8_t occ = 0;            // Сколько занимали у старшего разряда
    int i=0;                    // Счетчик

    /* Обнуляем вывод */
    bzero(out, MAXBYTELEN);

    /* По правилам, из большего вычитается меньшее, т.о. опредялем
     * какое из чисел меньшее */
    if (compare(minuend, subtrahend) < 0)
    {
        char tmp[MAXBYTELEN];
        sub(subtrahend, minuend, tmp);
        sprintf(out, "-%s", tmp);
        return;
    }

    /* Определяем длины строк */
    int maxLen = strlen(minuend), minLen = strlen(subtrahend);

    /* Производим действие до конца большей строки */
    for(i = 0; i < maxLen; i++)
    {
        /* Переменные содержат текущий разряд числа или '0',
         * если он не существует */
        uint8_t f = minuend[maxLen-i-1]-'0';
        uint8_t s = i < minLen ? subtrahend[minLen-i-1]-'0' : 0;

        /* Считаем разность */
        diff = f - s - occ;

        /* Определяем число больше или меньше 0 */
        if (diff < 0)
        {
            /* Занимаем 1 у старшего разряда, и производим вычисление */
            occ = 1;
            out[i] = (char)(10 + diff) + '0';
        }
        else
        {
            /* Занимать ненужно, отправляем итог в переменную вывода */
            occ = 0;
            out[i] = (char)diff + '0';
        }
    }

    /* Если последние числа 0, то удаляемих */
    while(out[i-1] == '0' && i > 1) i--;

    /* Терминируем строку (поскольку '0' не удялялись) */
    out[i] = 0;

    /* Разворачиваем число, поскольку оно заполнялось слева направо */
    revert(out);
}

/**
 * @brief Функция умножения целых положительных чисел
 * @param multiplier - Множимое
 * @param factor - Множитель
 * @param out - Указатель на вывод
 *
 * @remarks Уменьшаем множитель слева направа, чтобы умножать число,
 *          чтобы умножать число справа налева. Рекурсия выбрана
 *          для уменьшения необходимого числа переменных и кода функции.
 */
void mult(char *multiplier, char *factor, char *out)
{
    /* База рекурсии */
    if (strlen(factor) == 0)
    {
        out[0] = '0';
        out[1] = 0;
        return;
    }

    uint8_t over = 0;   // Переполнение, содержит первый разряд произведения
    int compos = 0; // Произведение разрядов (содержит нулевой и первый разряды)
    int i = 0;  // Счетчик
    int multiplierLen = strlen(multiplier);  // Длина множимого
    char out_cur[MAXBYTELEN];   // Строка для уже вычисленной части
    char tmp[MAXBYTELEN];       // Строка для вычисляемой части

    /* Обнуляем все строки */
    bzero(out_cur, MAXBYTELEN);
    bzero(tmp, MAXBYTELEN);

    /* Производим умножение меньшего разряда */
    mult(multiplier, factor+1, out);

    /* Передаем уже вычисленную часть в строку out-cur */
    strcpy(out_cur, out);
    bzero(out, MAXBYTELEN);

    /* Получаем цифру содержащуюся в текущем разряде множителя */
    uint8_t s = factor[0] - '0';

    /* Производим перемножение */
    for(i = 0; i < multiplierLen; i++)
    {
        /* Переменная содержит текущий разряд множимого */
        uint8_t f = multiplier[multiplierLen-i-1] - '0';

        /* Произведение разрядов */
        compos = f * s + over;

        /* 1 Разряд отправляется в переполнение */
        over = compos / 10;

        /* 0 разряд отправляется в переменную вывода */
        tmp[i] = (char)(compos % 10) + '0';
    }

    /* Если в переменной переполнения содрежится значение, то оно
     * отправляется в переменную вывода */
    if (over) tmp[i++] = over + '0';

    /* Разворачиваем число, поскольку оно заполнялось слева направо */
    revert(tmp);

    /* Добавляем нули в в конец строки соответственно длине множителя
     * (текущему разряду) */
    for (i = 0; i < (int)strlen(factor) - 1; i++)
        tmp[strlen(tmp)] = '0';

    /* Складываем уже вычисленную часть и вычисляемую, ответ
     * отправляем в переменную вывода */
    add(out_cur, tmp, out);
}

/**
 * @brief Функция деления целых положительных чисел
 * @param devisible - Делимое
 * @param devidor - Делитель
 * @param out - Указатель на вывод
 */
void divis(char *devisible, char *devidor, char *out)
{
    char cur_devisible[MAXBYTELEN]; // Текущее состояние делимого

    /* Обнуляем вывод */
    bzero(out, MAXBYTELEN);

    /* Копируем делимое */
    strcpy(cur_devisible, devisible);

    /* Делим */
    while(compare(cur_devisible, devidor) >= 0)
    {
        int i = 0;                      // Счетчик
        char tmp[MAXBYTELEN];           // Временная переменная, для определения
                                        // частного
        char subtrahend[MAXBYTELEN];    // Строка содержащая вычитаемую часть
        char cur_quotient[MAXBYTELEN];  // Текущее состояние частного

        /* Обнуление строк */
        bzero(cur_quotient, MAXBYTELEN);
        bzero(tmp, MAXBYTELEN);
        bzero(subtrahend, MAXBYTELEN);

        /* Если делитель больше делимого, останавливаем вычисление */
        if (compare(cur_devisible, devidor) < 0)
            return;

        /* Получаем число из делимого, которое больше делителя,
         * начиная с большего разряда, например для числа 12457 : 415
         * таким числом будет 1245 */
        while(compare(tmp, devidor) < 0)
            tmp[strlen(tmp)] = cur_devisible[i++];

        /* Ищем cur_quotient такой, что devidor * cur_quotient > tmp */
        i = 0;
        do
        {
            /* Делаем перебор от 1 до
             * devidor * cur_quotient > tmp */
            sprintf(cur_quotient, "%d", ++i);
            mult(cur_quotient, devidor, subtrahend);

            /* Данная ситуация не должна произойти */
            if (i == 10)
            {
                strcpy(out, "Devision error");
                return;
            }
        }
        while(compare(subtrahend, tmp) <= 0);

        /* Поскольку искали devidor * cur_quotient > tmp, откатываемся до
         * предидущего i */
        sprintf(cur_quotient, "%d", --i);

        /* Получаем произведение devidor * cur_quotient, которое меньше
         * либо равно tmp */
        mult(cur_quotient, devidor, subtrahend);

        /* Добавляем 0 согласно текущему разряду */
        while(strlen(subtrahend) < strlen(cur_devisible))
        {
            /* Добавляем 0 для вычитания из текущего делимого */
            subtrahend[strlen(subtrahend)] = '0';

            /* Определяем порядок текущего разряда частного */
            i *= 10;
        }

        /* Переводим текущее частное в строку */
        sprintf(cur_quotient, "%d", i);

        /* Получаем разность делимого и devidor * cur_quotient */
        sub(cur_devisible, subtrahend, tmp);

        /* Копируем полученное делимое из временной переменной в
         * соответствующую переменную */
        strcpy(cur_devisible, tmp);

        /* Копируем out во временную переменную */
        strcpy(tmp, out);

        /* Получаем новое состояние частного */
        add(tmp, cur_quotient, out);
    }
}

/**
 * @brief Функция сложения целых чисел
 * @param addendum1 - Слагаемое 1
 * @param addendum2 - Слагаемое 2
 * @param out - Указатель на вывод
 */
void addition(char *addendum1, char *addendum2, char *out)
{
    /* Получаем указатель на беззнаковую часть */
    char *addendum1New = addendum1[0] == '-' ? addendum1+1 : addendum1;
    char *addendum2New = addendum2[0] == '-' ? addendum2+1 : addendum2;
    char tmp[MAXBYTELEN];   // Временная строка

    /* Если первое слагаемое отрицательно */
    if (addendum1New != addendum1 && addendum2New == addendum2)
    {
        /* Вычитаем из второго первое */
        sub(addendum2New, addendum1New, out);
        return;
    }

    /* Если второе слагаемое отрицательно */
    if (addendum1New == addendum1 && addendum2New != addendum2)
    {
        /* Вычитаем из первого второе */
        sub(addendum1New, addendum2New, out);
        return;
    }

    /* Если оба слагаемых отрицательны */
    if (addendum1New != addendum1 && addendum2New != addendum2)
    {
        /* Складываем числа */
        add(addendum1New, addendum2New, tmp);

        /* Меняем знак */
        sprintf(out, "-%s", tmp);
        return;
    }

    /* Оба слагаемых положительны, складываем числа */
    add(addendum1New, addendum2New, out);
}

/**
 * @brief Функция вычитания целых чисел
 * @param minuend - Уменьшаемое
 * @param subtrahend - Вычитаемое
 * @param out - Указатель на вывод
 */
void substraction(char *minuend, char *subtrahend, char *out)
{
    /* Получаем указатель на беззнаковую часть */
    char *minuendNew = minuend[0] == '-' ? minuend+1 : minuend;
    char *subtrahendNew = subtrahend[0] == '-' ? subtrahend+1 : subtrahend;
    char tmp[MAXBYTELEN];   // Временная строка

    /* Если первое слагаемое отрицательно */
    if (minuendNew != minuend && subtrahendNew == subtrahend)
    {
        /* Складываем числа */
        add(minuendNew, subtrahendNew, tmp);

        /* Меняем знак */
        sprintf(out, "-%s", tmp);
        return;
    }

    /* Если второе слагаемое отрицательно */
    if (minuendNew == minuend && subtrahendNew != subtrahend)
    {
        /* Складываем числа */
        add(minuendNew, subtrahendNew, out);
        return;
    }

    /* Если оба слагаемых отрицательны */
    if (minuendNew != minuend && subtrahendNew != subtrahend)
    {
        /* Вычитаем из второго первое */
        sub(subtrahendNew, minuendNew, out);
        return;
    }

    /* Оба числа положительны, вычитаем из первого второе */
    sub(minuendNew, subtrahendNew, out);
}

/**
 * @brief Функция умножения целых положительных чисел
 * @param multiplier - Множимое
 * @param factor - Множитель
 * @param out - Указатель на вывод
 */
void multiplection(char *multiplier, char *factor, char *out)
{   
    /* Получаем указатель на беззнаковую часть */
    char *multiplierNew = multiplier[0] == '-' ? multiplier+1 : multiplier;
    char *factorNew = factor[0] == '-' ? factor+1 : factor;
    char tmp[MAXBYTELEN];   // Временная строка

    /* Если числа имеют разные знаки */
    if ((multiplierNew != multiplier && factorNew == factor) ||
        (multiplierNew == multiplier && factorNew != factor))
    {
        /* Перемножаем */
        mult(multiplierNew, factorNew, tmp);

        /* Меняем знак */
        sprintf(out, "-%s", tmp);
        return;
    }

    /* Числа имеют одинаковые знаки, перемножаем */
    mult(multiplierNew, factorNew, out);
}

/**
 * @brief Функция деления целых положительных чисел
 * @param devisible - Делимое
 * @param devidor - Делитель
 * @param out - Указатель на вывод
 */
void division(char *devisible, char *devidor, char *out)
{
    /* Получаем указатель на беззнаковую часть */
    char *devisibleNew = devisible[0] == '-' ? devisible+1 : devisible;
    char *devidorNew = devidor[0] == '-' ? devidor+1 : devidor;
    char tmp[MAXBYTELEN];   // Временная строка

    /* Если делитель равен 0 */
    if (strlen(devidor) == 1 && devidor[0] == '0')
    {
        strcpy(out, "Devision by zero");
        return;
    }

    /* Если делаитель больше делимого, при целочисленном делении получается 0 */
    if (compare(devisible, devidor) < 0)
    {
        out[0] = '0';
        out[1] = 0;
        return;
    }

    /* Если числа имеют разные знаки */
    if ((devisibleNew != devisible && devidorNew == devidor) ||
        (devisibleNew == devisible && devidorNew != devidor))
    {
        /* Делим */
        divis(devisibleNew, devidorNew, tmp);

        /* Меняем знак */
        sprintf(out, "-%s", tmp);
        return;
    }

    /* Числа имеют одинаковые знаки, делим */
    divis(devisibleNew, devidorNew, out);
}
